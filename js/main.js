document.getElementById('title')?.addEventListener('click', event => console.log(event.target))
document.getElementById('title')?.addEventListener('click', function (event) {
  console.log(event.target)
})

/**
 *
 * @param fullName (string)
 * @param callback (string, string) => number
 */
function processPerson(fullName, callback) {
  const nameParts = fullName.split(' ')
  const firstName = nameParts[0]
  const lastName = nameParts[1]
  const salary = callback(firstName, lastName)

  return {
    firstName,
    lastName,
    salary,
    doSomethingFunny() {
      // TODO: implement function
    }
  }
}

const person1 = processPerson('David Schwarz', (firstName, lastName) => 1000)
const person2 = processPerson('Max Muster', (firstName, lastName) => 3000)
const person3 = processPerson('Klara Fall', (firstName, lastName) => 10000)
console.log(person1)


const users = [
  person1,
  person2,
  person3
]
const filteredUsers = users.filter(function (person, index) {
  return person.salary < 5000
})
console.log(filteredUsers)

const indexPerson2 = users.findIndex(function (person) {
  return person.firstName === 'Max' && person.lastName === 'Muster'
})

const indexPerson2_Anders = users.includes(person2)
console.log(indexPerson2, indexPerson2_Anders)

users.splice(1, 0, {firstName: 'Hans', lastName: 'Hans', salary: 4000}, {firstName: 'Herbert', lastName: 'Huber', salary: 100})
console.log(users)










/*
{
  firstName: "David",
  lastName: "Schwarz",
  salary: 1000
}
 */

function accessDeepObjectProperty(obj) {
  console.log(obj?.user?.firstName)
}

accessDeepObjectProperty({x: { y: {} }})
